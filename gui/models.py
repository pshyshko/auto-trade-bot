from dataclasses import dataclass


@dataclass
class OrderLimit:
    name: str
    min_price: float
    max_price: float
    buy_item: bool

    def __str__(self):
        return (
            f"{self.name: <50} {self.buy_item: <5} "
            f"{self.min_price: <5} {self.max_price: <5}"
        )
