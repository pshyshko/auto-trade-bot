from operations import Operation
import PySimpleGUI as sg


operation = Operation()


def gui_main():

    oper = Operation()

    data, headings = oper.get_active_orders(730)

    layout_base = [
        [
            sg.Table(
                values=data[:],
                headings=headings,
                size=(60, 30),
                display_row_numbers=True,
                justification="left",
                key="_table_",
            )
        ]
    ]

    layout_buttons = [
        [sg.Button("Active orders", size=(10, 1), key="-ORDERS-")],
        [sg.Button("Limits", size=(10, 1), key="-LIMITS-")],
        [sg.Button("Logs", size=(10, 1), key="-LOGS-")],
    ]

    layout_main = [
        [
            sg.Column(
                layout_base,
                key="-MAIN_TABLE-",
            ),
            sg.Column(layout_buttons),
        ]
    ]

    window = sg.Window("", layout_main)
    return window


def gui_info_sales(name_item):
    data, headings = operation.get_sales_history(name_item)
    layout_base = [
        [
            sg.Table(
                values=data[:],
                headings=headings,
                size=(60, 30),
                display_row_numbers=True,
                justification="left",
                key="_table_",
            )
        ]
    ]

    layout_main = [
        [
            sg.Column(
                layout_base,
                key="-MAIN_TABLE-",
            )
        ]
    ]

    window = sg.Window("", layout_main)

    while True:
        event, values = window.read()

        if event == "Exit" or event == sg.WIN_CLOSED:
            break


def gui_info_item_by(name_item):
    data, headings = operation.get_item_by(name_item)
    layout_base = [
        [
            sg.Table(
                values=data[:],
                headings=headings,
                size=(60, 30),
                display_row_numbers=True,
                justification="left",
                key="_table_",
            )
        ]
    ]

    layout_main = [
        [
            sg.Column(
                layout_base,
                key="-MAIN_TABLE-",
            )
        ]
    ]

    window = sg.Window("", layout_main)

    while True:
        event, values = window.read()

        if event == "Exit" or event == sg.WIN_CLOSED:
            break


def gui_limit():
    data, headings = operation.get_list_limits(730)
    layout_base = [
        [
            sg.Table(
                values=data[:],
                headings=headings,
                size=(60, 30),
                display_row_numbers=True,
                justification="left",
                key="_table_",
            )
        ]
    ]

    layout_buttons = [
        [sg.Button("My inventory", size=(12, 1), key="-INVENTORY-")],
        [sg.Button("Add item", size=(12, 1), key="-ADD ITEM-")],
        [sg.Button("Change item", size=(12, 1), key="-CHANGE ITEM-")],
        [sg.Button("Delete item", size=(12, 1), key="-DELETE ITEM-")],
        [sg.Button("Info item", size=(12, 1), key="-SCAN-")],
        [sg.Button("Last sold", size=(12, 1), key="-SOLDS-")]
    ]

    layout_main = [
        [
            sg.Column(
                layout_base,
                key="-MAIN_TABLE-",
            ),
            sg.Column(layout_buttons),
        ]
    ]

    window = sg.Window("", layout_main)
    return window


def gui_logs():
    oper = Operation()
    data, headings = oper.get_logs_change(730)

    layout_base = [
        [
            sg.Table(
                values=data[:],
                headings=headings,
                size=(60, 30),
                display_row_numbers=True,
                justification="left",
                key="_table_",
            )
        ]
    ]

    layout_buttons = [
        [sg.Button("Active orders", size=(10, 1), key="-ORDERS-")],
        [sg.Button("Limits", size=(10, 1), key="-LIMITS-")],
        [sg.Button("Logs", size=(10, 1), key="-LOGS-")],
    ]

    layout_main = [
        [
            sg.Column(
                layout_base,
                key="-MAIN_TABLE-",
            ),
            sg.Column(layout_buttons),
        ]
    ]

    window = sg.Window("", layout_main)
    return window


def gui_add_item():
    layout_input_limits = [
        [
            sg.Text("Name"),
            sg.In(size=(20, 1)),
            sg.Text("Min price"),
            sg.In(size=(6, 1)),
            sg.Text("Max price"),
            sg.In(size=(6, 1)),
            sg.Checkbox('Available for sale'),
            sg.Button("Save", size=(5, 1), key="-SAVE_LIMIT-")
        ]
    ]

    layout_main = [
        [
            sg.Column(
                layout_input_limits,
                key="-MAIN_TABLE-",
            )
        ]
    ]
    window = sg.Window("", layout_main)

    while True:
        event, values = window.read()
        print(event, values)

        if event == "Exit" or event == sg.WIN_CLOSED:
            break

        if event == "-SAVE_LIMIT-":
            window.close()
            values = list(values.values())
            try:
                name_item = values[0].strip()
                min_price = float(values[1])
                max_price = float(values[2])
                buy = values[3]

                operation.add_new_limit(name_item, min_price, max_price, buy)

            except Exception:
                continue
            break


def gui_change_limit_price(order: list):
    layout_input_limits = [
        [
            sg.Text(order[0]),
            sg.Text("Min price"),
            sg.In(size=(6, 1)),
            sg.Text("Max price"),
            sg.In(size=(6, 1)),
            sg.Checkbox('Available for sale'),
            sg.Button("Save", size=(5, 1), key="-SAVE_LIMIT-")
        ]
    ]

    layout_main = [
        [
            sg.Column(
                layout_input_limits,
                key="-MAIN_TABLE-",
            )
        ]
    ]

    window = sg.Window("", layout_main)

    while True:
        event, values = window.read()
        print(event, values)

        if event == "Exit" or event == sg.WIN_CLOSED:
            break

        if event == "-SAVE_LIMIT-":
            window.close()
            values = list(values.values())
            try:

                min_price = float(values[0])
                max_price = float(values[1])
                buy = values[2]
                operation.update_limit(order[0], min_price, max_price, buy)
            except Exception:
                continue
            break


window = gui_limit()


while True:
    event, values = window.read()
    print(event, values)
    if event == "Exit" or event == sg.WIN_CLOSED:
        break

    # if event == "-ORDERS-":
    #     window.close()
    #     window = gui_main()

    if event == "-LIMITS-":
        window.close()
        window = gui_limit()

    if event == "-INVENTORY-":
        ...

    if event == "-ADD ITEM-":
        gui_add_item()
        window.close()
        window = gui_limit()

    if event == "-DELETE ITEM-":
        try:
            row = values['_table_'][0]
            order = window['_table_'].Get()[row]
            operation.delete_item(order[0])
            window.close()
            window = gui_limit()
        except Exception:
            pass
    if event == "-CHANGE ITEM-":
        try:
            row = values['_table_'][0]
            order = window['_table_'].Get()[row]
            gui_change_limit_price(order)
            window.close()
            window = gui_limit()

        except Exception:
            pass
    if event == "-SCAN-":
        try:
            row = values['_table_'][0]
            order = window['_table_'].Get()[row]
            name = order[0]
            gui_info_item_by(name)

        except Exception:
            pass

    if event == "-SOLDS-":
        row = values['_table_'][0]
        order = window['_table_'].Get()[row]
        name = order[0]
        gui_info_sales(name)

    # if event == "-LOGS-":
    #     window.close()
    #     window = gui_logs()

window.close()
