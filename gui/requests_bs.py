import pyotp
import requests
from config import Setting

SECRET_TOKEN = Setting.SECRET_TOKEN
TOKEN_API = Setting.TOKEN_API
LINK_BITSKINS = "https://bitskins.com"


def generate_key():
    return pyotp.TOTP(SECRET_TOKEN).now()


def get_active_buy_orders(app_id):
    link = LINK_BITSKINS + "/api/v1/get_active_buy_orders"

    params = {"api_key": TOKEN_API, "code": generate_key(), "app_id": app_id}

    response = requests.post(link, params=params)

    return response.json()


def cancel_buy_orders(buy_order_ids, app_id):
    link = LINK_BITSKINS + "/api/v1/cancel_buy_orders"

    params = {
        "api_key": TOKEN_API,
        "code": generate_key(),
        "buy_order_ids": buy_order_ids,
        "app_id": app_id,
    }
    return requests.post(link, params=params)


def get_place_in_queue(self, name, price, app_id):
    link = self.LINK_BITSKINS
    link += "/api/v1/get_expected_place_in_queue_for_new_buy_order"
    headers = {
        "User-Agent": (
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64;"
            "rv:80.0) Gecko/20100101 Firefox/80.0"
        )
    }

    params = {
        "api_key": self.TOKEN_API,
        "code": self.generate_key(),
        "name": name,
        "price": price,
        "app_id": app_id,
    }

    response = requests.post(link, headers=headers, params=params)

    return response.json()


def get_place_in_queue_v2(self, name, price, app_id, headers):
    link = self.LINK_BITSKINS
    link += +"/api/v1/get_expected_place_in_queue_for_new_buy_order"

    params = {
        "api_key": self.TOKEN_API,
        "code": self.generate_key(),
        "name": name,
        "price": price,
        "app_id": app_id,
    }

    response = requests.post(link, headers=headers, params=params)

    return response.json()


def get_market_buy_order(market_hash_name, app_id):
    link = LINK_BITSKINS + "/api/v1/get_market_buy_orders"

    params = {
        "api_key": TOKEN_API,
        "code": generate_key(),
        "market_hash_name": market_hash_name,
        "app_id": app_id,
    }

    response = requests.post(link, params=params)

    return response.json()


def get_market_buy_orders(app_id):
    link = LINK_BITSKINS + "/api/v1/get_market_buy_orders"

    params = {"api_key": TOKEN_API, "code": generate_key(), "app_id": app_id}
    response = requests.post(link, params=params)

    return response.json()


def create_buy_order(name, price, app_id):
    link = LINK_BITSKINS + "/api/v1/create_buy_order"

    params = {
        "api_key": TOKEN_API,
        "code": generate_key(),
        "name": name,
        "price": price,
        "app_id": app_id,
    }

    return requests.post(link, params=params)


def get_price_data_for_items_on_sale(app_id):
    link = LINK_BITSKINS + "/api/v1/get_price_data_for_items_on_sale"

    params = {"api_key": TOKEN_API, "code": generate_key(), "app_id": app_id}
    response = requests.post(link, params=params)
    response = response.json()

    return response["data"]["items"]


def get_steam_price(app_id):
    link = LINK_BITSKINS + "/api/v1/get_steam_price_data"

    params = {"api_key": TOKEN_API, "code": generate_key(), "app_id": app_id}

    response = requests.post(link, params=params)
    return response


def get_settled_buy_orders(app_id):
    link = LINK_BITSKINS + "/api/v1/get_settled_buy_orders"
    params = {"api_key": TOKEN_API, "code": generate_key(), "app_id": app_id}
    response = requests.post(link, params=params)
    return response.json()


def get_inventory_on_sale(app_id,
                          market_hash_name: str,
                          min_price: float,
                          max_price: float):

    link = LINK_BITSKINS + "/api/v1/get_inventory_on_sale"
    params = {"api_key": TOKEN_API,
              "code": generate_key(),
              "app_id": app_id,
              "sort_by": "price",
              "order": "asc",
              "market_hash_name": market_hash_name,
              "min_price": min_price,
              "max_price": max_price}

    response = requests.post(link, params=params)
    return response.json()


def get_inventory_on_sale_all(app_id,
                              market_hash_name: str):

    link = LINK_BITSKINS + "/api/v1/get_inventory_on_sale"
    params = {"api_key": TOKEN_API,
              "code": generate_key(),
              "app_id": app_id,
              "sort_by": "price",
              "order": "asc",
              "market_hash_name": market_hash_name}

    response = requests.post(link, params=params)
    return response.json()


def buy_item(app_id, prices, item_ids):
    link = LINK_BITSKINS + "/api/v1/buy_item"

    params = {"api_key": TOKEN_API,
              "code": generate_key(),
              "app_id": app_id,
              "item_ids": item_ids,
              "prices": prices,
              "allow_trade_delayed_purchases": "true"}

    return requests.get(link, params=params).json()


def get_sales_info(market_hash_name: str):

    link = LINK_BITSKINS + "/api/v1/get_sales_info"
    params = {"api_key": TOKEN_API,
              "code": generate_key(),
              "market_hash_name": market_hash_name}

    response = requests.post(link, params=params)
    return response.json()


if __name__ == "__main__":
    print(get_sales_info('AWP | Wildfire (Field-Tested)'))
    # print(buy_item(730, prices, ids))
