from pymongo import MongoClient
from models import OrderLimit
from requests_bs import get_active_buy_orders, get_inventory_on_sale_all, get_sales_info
import config
from datetime import datetime
# import sys
# link = os.path.dirname(sys.path[0]) + '\\app'
# sys.path.insert(0, link)


class Operation:
    def __init__(self):
        LINK = config.Setting.LINK
        cluster = MongoClient(LINK)

        self.db = cluster["bitskins"]

    def get_active_orders(self,
                          app_id: int,
                          convert_table: bool = False) -> tuple:

        orders = get_active_buy_orders(app_id)
        orders = orders["data"]["orders"]
        keys = [
            "buy_order_id",
            "market_hash_name",
            "price",
            "created_at",
            "place_in_queue",
        ]

        values = []
        for order in orders:
            values.append([order[i] for i in keys])

        return values, keys

    def get_list_limits(self, convert_table: bool = False) -> tuple:
        docs = list(self.db["limit_price"].find({}))
        keys = ["market_hash_name", "min_price", "max_price", "buy_item"]
        values = []
        for doc in docs:
            temp = list(doc.values())[1:]
            OrderLimit(*temp)
            values.append([doc[i] for i in keys])

        return values, keys

    def get_logs_change(self, convert_table: bool = False) -> tuple:
        docs = list(self.db["log_change_price"].find({}))
        keys = [
            "buy_order_id",
            "market_hash_name",
            "old_price",
            "new_price",
            "created_at",
            "place_in_queue",
        ]

        values = []
        for doc in docs:
            values.append([doc[i] for i in keys])

        return values, keys

    def update_limit(self,
                     name_item: str,
                     min: float,
                     max: float,
                     buy: bool) -> None:

        key = {"market_hash_name": name_item}
        change_value = {"$set": {"min_price": min,
                                 "max_price": max,
                                 "buy_item": buy}}

        self.db["limit_price"].update_one(key, change_value)

    def add_new_limit(self,
                      name_item: str,
                      min: float,
                      max: float,
                      buy: bool) -> None:

        data = {"market_hash_name": name_item,
                "min_price": min,
                "max_price": max,
                "buy_item": buy}

        self.db["limit_price"].insert_one(data)

    def delete_item(self, name_item: str):
        query = {"market_hash_name": name_item}
        self.db['limit_price'].delete_one(query)

    def get_item_by(self, name_item: str):
        docs = get_inventory_on_sale_all(730, name_item)['data']['items']
        keys = ["market_hash_name", "price", "suggested_price"]
        values = []
        for doc in docs:
            values.append([doc[i] for i in keys])

        return values, keys

    def get_sales_history(self, name_item: str):
        docs = get_sales_info(name_item)['data']['sales']
        keys = ["market_hash_name", "price", "sold_at"]
        values = []
        for doc in docs:
            temp = [doc[i] for i in keys]
            temp[2] = datetime.fromtimestamp(temp[2])
            temp[2] = temp[2].strftime("%d/%m/%Y %H:%M:%S")
            values.append(temp)

        return values, keys


if __name__ == "__main__":
    a = Operation()
    print(a.get_sales_history('AWP | Wildfire (Field-Tested)'))
