from pymongo import MongoClient
from models import OrderLimits
from datetime import datetime
import os
import logging
import requests_bs as r
import concurrent.futures as cf
import requests


logging.basicConfig(
    # handlers=[logging.FileHandler(filename=".
    # /log_records.txt", encoding='utf-8', mode='a+')],
    format="%(asctime)s %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S"
)


class AutoBuyer:
    def __init__(self, app_id):
        NAME_DB = os.environ["BITSKINS_NAME_DB"]
        ACCOUNT_DB = os.environ["BITSKINS_ACCOUNT_DB"]
        PASSWORD_DB = os.environ["BITSKINS_PASSWORD"]

        LINK = (
            f"mongodb+srv://{ACCOUNT_DB}:{PASSWORD_DB}@botdata.ibf0z."
            f"mongodb.net/{NAME_DB}?retryWrites=true&w=majority"
        )
        cluster = MongoClient(LINK)
        self.app_id = app_id
        self.db = cluster["bitskins"]
        self.tasks = []
        self.session = None

    def get_limits_items(self) -> list:
        docs = self.db["limit_price"].find({})
        return docs

    def get_info_item_by_name(self,
                              name: str,
                              min_price: float,
                              max_price: float) -> float:
        try:
            items = r.get_inventory_on_sale_session(self.app_id,
                                                    name,
                                                    min_price,
                                                    max_price,
                                                    self.session)

            result_len = len(items["data"]["items"])
            if result_len != 0:
                item = items["data"]["items"][0]
                id = item["item_id"]
                cost = round(float(item["price"]), 2)
                return name, id, cost
        except Exception:
            return None

    def processing_order(self, order):
        logging.info(order)
        if order.buy_item:
            value = self.get_info_item_by_name(
                order.name, order.min_price, order.max_price
            )

            if value is not None:
                logging.warning(f"Buy item: {value[0]} {value[2]}")

                # debug
                # self.tasks.append((print, values))
                # buy_item(self.app_id, value[1], value[2])

                # main function for buy
                values = (self.app_id, value[1], value[2])
                self.tasks.append((r.buy_item, values))

                key = {"market_hash_name": order.name}
                change_value = {"$set": {"buy_item": False}}
                self.db["limit_price"].update_one(key, change_value)

    def run(self):
        while True:
            print('#'*100)
            start = datetime.now()

            docs = self.get_limits_items()
            orders = [OrderLimits(*list(doc.values())[1:]) for doc in docs]

            with cf.ThreadPoolExecutor() as executor:
                with requests.Session() as session:
                    self.session = session
                    futures = executor.map(self.processing_order, orders)

            for result in futures:
                pass

            print('*'*100)
            if len(self.tasks) != 0:

                for task in self.tasks:
                    print(task)
                    task[0](*task[1])
                self.tasks = []

            # for order in orders:
            #    self.processing_order(order)

            end = datetime.now() - start
            logging.info(end)
            print('*'*100)


bot = AutoBuyer(730)
bot.run()
