from apscheduler.schedulers.blocking import BlockingScheduler
from pytz import utc
from pymongo import MongoClient
from models import Order
from typing import Union
import time
import os
import logging
import requests_bs as req_bs
import concurrent.futures as cf


logging.basicConfig(
    format="%(asctime)s %(message)s", level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S"
)


class autoOrders:
    def __init__(self, app_id):
        NAME_DB = os.environ["BITSKINS_NAME_DB"]
        ACCOUNT_DB = os.environ["BITSKINS_ACCOUNT_DB"]
        PASSWORD_DB = os.environ["BITSKINS_PASSWORD"]

        LINK = (
            f"mongodb+srv://{ACCOUNT_DB}:{PASSWORD_DB}@botdata.ibf0z."
            f"mongodb.net/{NAME_DB}?retryWrites=true&w=majority"
        )
        cluster = MongoClient(LINK)
        self.app_id = app_id
        self.db = cluster["bitskins"]

    def _log_action(self, task: str, mess: Union[str, Order]):
        try:
            messeng = f"action: [{task}] > {mess}"
            logging.info(messeng)
        except Exception:
            logging.error("Error with logs")

    def get_bought_orders(self) -> list[dict]:
        respond = req_bs.get_settled_buy_orders(self.app_id)
        docs = respond["data"]["orders"]
        orders = [item for item in docs
                  if not item["settled_with_item"]["withdrawn"]]
        return orders

    def get_active_orders(self) -> list[dict]:
        respond = req_bs.get_active_buy_orders(self.app_id)
        orders = [item for item in respond["data"]["orders"]]
        return orders

    def get_advantage_price_in_queue(
        self, name_item, min_limit_price, max_limit_price
    ) -> float:
        """[summary]

        Algorithm for finding the best price with
        the help of the first buyer's position at the auction

        """

        respond = req_bs.get_market_buy_order(name_item, self.app_id)
        orders = respond["data"]["orders"]

        orders = [
            item
            for item in orders
            if round(float(item["price"]), 2) >= min_limit_price
            and round(float(item["price"]), 2) <= max_limit_price
        ]

        total = len(orders)
        if total >= 1:

            is_mine = orders[0]["is_mine"]
            currect_price = float(orders[0]["price"])

            if not is_mine or currect_price > max_limit_price:
                for item in orders[::-1]:
                    if float(item["price"]) < max_limit_price:
                        temp = float(item["price"])
                        price = round(temp + 0.01, 2)
                    else:
                        break

            if is_mine and total >= 2:
                second_orders = orders[1]
                second_order_price = round(float(second_orders["price"]), 2)
                if second_order_price >= min_limit_price:
                    price = round(second_order_price + 0.01, 2)

        if price is None or price < min_limit_price or price > max_limit_price:
            return min_limit_price
        else:
            return round(float(price), 2)

    def get_current_place(self, name_item):
        respond = req_bs.get_market_buy_order(name_item, self.app_id)
        orders = respond["data"]["orders"]
        my_order = [item for item in orders if item["is_mine"]]
        place = my_order[0]["place_in_queue"]
        return place

    def get_limit_price(self, name_item):
        respond = self.db["limit_price"].find({"market_hash_name": name_item})
        try:
            return respond[0]["max_price"]
        except Exception:
            return 0.0

    def get_limit_min_price(self, name_item):
        respond = self.db["limit_price"].find({"market_hash_name": name_item})
        try:
            return respond[0]["min_price"]
        except Exception:
            return 0.0

    def send_log_change_orders(self, item, new_price):

        data = {
            "buy_order_id": item["buy_order_id"],
            "market_hash_name": item["market_hash_name"],
            "old_price": float(item["price"]),
            "new_price": new_price,
            "suggested_price": item["suggested_price"],
            "created_at": item["created_at"],
            "place_in_queue": item["place_in_queue"],
        }

        self.db["log_change_price"].insert_one(data)

    def send_sold_orders(self):
        temp = req_bs.get_settled_buy_orders(self.app_id)
        temp = temp["data"]["orders"]
        temp = [item for item in temp
                if not item["settled_with_item"]["withdrawn"]]

        for item in temp:
            key = {"buy_order_id": item["buy_order_id"]}
            data = item
            self.log_bought_orders.update(key, data, upsert=True)

    def write_order_to_db(self, item):
        data_2 = {
            "market_hash_name": item["market_hash_name"],
            "min_price": None,
            "max_price": None,
        }

        self.db["limit_price"].insert_one(data_2)

    def handle_order(self, order):
        exsit_in_col = (
            self.db["limit_price"].count_documents(
                {"market_hash_name": order["market_hash_name"]}
            )
            == 0
        )

        if exsit_in_col:
            self.write_order_to_db(order)

        order = Order(
            order["market_hash_name"],
            order["buy_order_id"],
            round(float(order["price"]), 2),
            order["place_in_queue"],
            self.get_current_place(order["market_hash_name"]),
            self.get_limit_min_price(order["market_hash_name"]),
            self.get_limit_price(order["market_hash_name"]),
        )

        if order.max_price == 0 or order.max_price is None:
            print("you don't define limit of cost ")
            return

        self._log_action("checking", order)

        if order.price <= order.max_price:
            new_price = self.get_advantage_price_in_queue(
                order.name, order.min_price, order.max_price
            )

            if order.price != new_price:
                req_bs.cancel_buy_orders(order.id, self.app_id)

                messeng = (f"{order.id} {order.name} {order.price} -> "
                           f"{new_price}")

                self._log_action("switch price", messeng)

                req_bs.create_buy_order(order.name, new_price, self.app_id)
                # self.send_log_change_orders(order, new_price)

            else:
                pass
        else:
            pass

    def main(self):
        start_time = time.localtime()
        items_by = self.get_active_orders()

        # война за лимиты, архивируем
        # for item in items_by:
        #    self.handle_order(item)

        with cf.ThreadPoolExecutor() as executor:
            executor.map(self.handle_order, items_by)

        end_time = time.localtime() - start_time
        logging.info(f"The runtime is completed in {end_time}")

        named_tuple = time.localtime()
        time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
        print(f"End : {time_string}")

# debug


bot = autoOrders(730)
sched = BlockingScheduler(timezone=utc)


@sched.scheduled_job("interval", minutes=2)
def main_run():
    bot.main()


sched.start()
