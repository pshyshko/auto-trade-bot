from dataclasses import dataclass


@dataclass
class Order:
    name: str
    id: int
    price: float
    current_place: int
    next_place: int
    min_price: float
    max_price: float

    def __str__(self):
        return (
            f"{self.id: <9} {self.name: <50} {self.price: <5} "
            f"{self.current_place: <2} {self.next_place:<2} "
            f"{self.min_price: <5} {self.max_price: <5}"
        ).decode('ascii')


@dataclass
class OrderLimits:
    name: str
    min_price: float
    max_price: float
    buy_item: bool

    def __str__(self):
        return (
            f"{self.name: <50} {self.buy_item: <5} "
            f"{self.min_price: <5} {self.max_price: <5}"
        )
