FROM python:3.9.1


COPY . .
RUN pip install -r requirements.txt

CMD [ "python","-u", "app/auto_buyer.py" ]
